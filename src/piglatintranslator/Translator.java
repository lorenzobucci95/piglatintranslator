package piglatintranslator;

public class Translator {
	
	private String phrase;
	public final static String NIL = "nil";
	public final static String EMPTYSTRING = "";

	public Translator(String inputPhrase) {
		phrase = inputPhrase;
	}

	public String getphrase() {
		return phrase;
	}
	
	public String translate() {
		String phraseResult;
		String[] wordWithoutDash = null;
		String[] wordsWithoutSpaces = phrase.split(" ");
		
		for(int i=0; i<wordsWithoutSpaces.length; i++) {
			if(wordsWithoutSpaces[i].contains("-")) {
				wordWithoutDash = wordsWithoutSpaces[i].split("-");
				for(int j=0;j<wordWithoutDash.length;j++) {
					if(wordContainPunctuations(wordWithoutDash[j])) {
						wordWithoutDash[j] = translateWordWithPunctuations(wordWithoutDash[j]);
					} else {
						wordWithoutDash[j] = translateWordRules(wordWithoutDash[j]);
					}
				}
				wordsWithoutSpaces[i] = String.join("-", wordWithoutDash);
			} else {
				if(wordContainPunctuations(wordsWithoutSpaces[i])) {
					wordsWithoutSpaces[i] = translateWordWithPunctuations(wordsWithoutSpaces[i]);
				}else {
					wordsWithoutSpaces[i] = translateWordRules(wordsWithoutSpaces[i]);
				}
			}
		}
		phraseResult = String.join(" ", wordsWithoutSpaces);
		return phraseResult;
	}
	
	public String translateWordWithPunctuations(String word) {
		String wordWithoutPunctuationsTranslate = "";
		String finalWord="";
		
		if(!(word.matches("[:.;,-?() ]+"))) {
			if( (isLetterPunctuations(word.charAt(0)) && (isLetterPunctuations(word.charAt(word.length()-1))))) {
				wordWithoutPunctuationsTranslate = translateSubStringWord(word, getIndexSubStringStart(word), getIndexSubStringEnd(word));
				finalWord = getPunctuationsOnBeginning(word) + wordWithoutPunctuationsTranslate + reverseWord(getPunctuationsOnBeginning(reverseWord(word)));
			}else if(isLetterPunctuations(word.charAt(0))) {
				wordWithoutPunctuationsTranslate = translateSubStringWord(word,getIndexSubStringStart(word),word.length());
				finalWord = getPunctuationsOnBeginning(word) + wordWithoutPunctuationsTranslate;
			} else if(isLetterPunctuations(word.charAt(word.length()-1))) {
				wordWithoutPunctuationsTranslate = translateSubStringWord(word, 0,getIndexSubStringEnd(word));
				finalWord = wordWithoutPunctuationsTranslate + reverseWord(getPunctuationsOnBeginning(reverseWord(word)));
			}
		} else {
			finalWord = word;
		}
		return finalWord;
	}
	
	public String translateWordRules(String word) {
		String wordResult = null;
		
		if(startWithVowel(word)) {
			if(word.endsWith("y")) {
				wordResult =  word + "nay";
			} else if(endWithVowel(word)) {
				wordResult =  word + "yay";
			} else if(!endWithVowel(word)) {
				wordResult = word + "ay";
			}
		} else if(word.equals(EMPTYSTRING)) {
			wordResult = NIL;
		} else if(!startWithVowel(word)) {
			if(isSecondLetterVowel(word)) {
				wordResult = removeFirstLetter(word) + getFirstLetter(word) + "ay";
			}else if(getNumberOfConsonantsFromBeginning(word)>1) {
				wordResult = getPhraseWithoutConsonantsOnBeginning(word) + getConsonantsFromBeginning(word) + "ay";
			}
		}
		
		return wordResult;
	}
	
	private String reverseWord(String word) {
		StringBuilder strb = new StringBuilder(word);
		word = strb.reverse().toString();
		return word;
	}
	
	private String getPunctuationsOnBeginning(String word){
		int i = 0;
		StringBuilder punctuations = new StringBuilder();
		
		while(isLetterPunctuations(word.charAt(i))){
			punctuations.append(word.charAt(i));
			i++;
		}
		return punctuations.toString();
	}
	
	private int getIndexSubStringStart(String word) {
		int indexSubstringStart = 0;
		while(isLetterPunctuations(word.charAt(indexSubstringStart))) {
			indexSubstringStart++;
		}
		return indexSubstringStart;
	}
	
	private int getIndexSubStringEnd(String word) {
		int indexSubStringEnd = word.length()-1;
		while(isLetterPunctuations(word.charAt(indexSubStringEnd))) {
			indexSubStringEnd--;
		}
		return indexSubStringEnd+1;
	}
	
	private String translateSubStringWord(String word, int indexSubStringStart,int indexSubStringEnd) {
		String substring="";
		substring = translateWordRules(word.substring(indexSubStringStart,indexSubStringEnd));
		return substring;
	}
	
	private boolean isLetterPunctuations(char letter) {
		return ( (letter=='.') || (letter==',') || (letter==':') || (letter=='?') || (letter=='!') || (letter=='\'') || (letter=='(') || (letter==')'));
	}
	
	private boolean wordContainPunctuations(String word){
		return ( (word.contains(".")) || (word.contains(",")) || (word.contains(":")) || (word.contains("?")) || (word.contains("!")) || (word.contains("'")) || (word.contains("(")) || (word.contains(")")));
	}
	
	private boolean startWithVowel(String word) {
		return word.startsWith("a") || word.startsWith("e") || word.startsWith("i") || word.startsWith("o") || word.startsWith("u");
	}
	
	private boolean endWithVowel(String word) {
		return word.endsWith("a") || word.endsWith("e") || word.endsWith("i") || word.endsWith("o") || word.endsWith("u");
	}
	
	private char getPhraseSecondLetter(String word) {
		return word.charAt(1);
	}
	
	private boolean isSecondLetterVowel(String word) {
		return ((getPhraseSecondLetter(word)=='a') || (getPhraseSecondLetter(word)=='e') || (getPhraseSecondLetter(word)=='i') || (getPhraseSecondLetter(word)=='o') || (getPhraseSecondLetter(word)=='u'));
	}
	
	private String removeFirstLetter(String word) {
		return word.substring(1);
	}
	
	private char getFirstLetter(String word) {
		return word.charAt(0);
	}
	
	private String getConsonantsFromBeginning(String word){
		StringBuilder bld = new StringBuilder();
		String consonants = "";
		int i = 0;
		
		while((word.charAt(i)!='a') && (word.charAt(i)!='e') && (word.charAt(i)!='i') && (word.charAt(i)!='o') && (word.charAt(i)!='u') && (i<word.length())) {
			bld.append(word.charAt(i));
			i++;
		}
		consonants = bld.toString();
		return consonants;
	}
	
	private int getNumberOfConsonantsFromBeginning(String word) {
		int i = 0;
		
		while((word.charAt(i)!='a') && (word.charAt(i)!='e') && (word.charAt(i)!='i') && (word.charAt(i)!='o') && (word.charAt(i)!='u') && (i<word.length())) {
			i++;
		}
		return i;
	}
	
	private String getPhraseWithoutConsonantsOnBeginning(String word) {
		return word.substring(getNumberOfConsonantsFromBeginning(word));
	}
	
	

}
